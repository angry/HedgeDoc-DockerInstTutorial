# Tutorial zur Installation von HedgeDoc in der Docker-Variante

## Docker-Host erstellen

Basis war hier ein Debian 11 Bullseye (noch in Testing-Phase) in der Grundinstallation mit SSH-Zugang. Installation auf Basis der [Docker-CE-Anleitung](https://docs.docker.com/engine/install/debian/).

### Docker-CE installieren

```
#!/bin/bash

apt-get -y remove docker docker-engine docker.io containerd runc
apt-get update
apt-get -y install apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io
docker run hello-world
```

Am Ende des Skripts wird ein kleines Docker-Test-Image heruntergeladen und gestartet. Die Ausgabe enthält u.a. den Text `Hello from Docker!`. Damit ist Docker-CE installiert.

### Docker-Compose

Ermöglicht die Definition und Nutzung einer Steuerdatei, die es u.a. erlaubt komfortable mehrere Docker-Images quasi als Serververbund zu starten, virtuelle Netze zwischen den Servern zu haben, exportierbare Teile aus deren Dateisystem auf Verzeichnisse im Host abzubilden (z.B. um bei Updates des Docker-Images die Daten persistent zu behalten), ... .

```
#!/bin/bash

apt-get -y install curl
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

Resultat am Ende sollte eine Ausgabe wie `docker-compose version 1.29.2, build 5becea4c` sein.

### Docker-Host & Compose - all-in-one

Im Skript `install-docker-allinone` sind die beiden Shellskripte zur Installation von Docker CE und docker-compose in ein Skript zusammengefasst.

## HedgeDoc via Docker

In der [HedgeDoc-Dokumentation für Docker-basierende Installation] ist eine empfohlene YAML-Steuerdatei für `docker-compose` dokumentiert. Fast die gleiche Steuerdatei (`docker-compose.yml`), die aber z.B. noch ein internes Backend-Netzwerk zwischen dem Datenbank- und dem HedgeDoc-Server definiert, kann zusammen mit weiteren Tools aus einem GitHub-Repository geclont werden:

```
cd /opt
git clone https://github.com/hedgedoc/container.git hedgedoc-container
cd hedgedoc-container
```

Die Datei `docker-compose.yml` im neuen Verzeichnis `hedgedoc-container` editieren und auf eigene Bedürfnisse anpassen. Am Anfang wäre für einen Produktions-Server mal nur die Individualisierung des Datenbank-Passworts in der Variablen `POSTGRES_PASSWORD` empfohlen. Bitte beachten, dass das **gleiche** Passwort dann auch in der Variablen `CMD_DB_URL` hinterlegt sein muss.

Außerdem die beiden `image:`-Zeilen beachten. Die erste definiert die Nutzung Alpine-Linux-basierenden PostgreSQL-Servers, die zweite `image:`-Zeile definiert das zu verwendende HedgeDoc-Image. Hier ist für Produktions-Server das Debian-basierende Image empfohlen. Welche Versionen hier verfügbar sind, findet man unter [https://quay.io/repository/hedgedoc/hedgedoc?tab=tags](https://quay.io/repository/hedgedoc/hedgedoc?tab=tags). Ggf. also hier speziell die zweite `image:`-Zeile auf die gewünschte Version anpassen - z.B. auch bei einem Upgrade auf eine neue Version.

Zudem muss noch die Umgebungs-Variable `CMD_DOMAIM=localhost` mit einem eigenen Domain-Name versehen werden. Sofern der vorgeschaltete Reverse Proxy Hedgedoc via HTTPS ausliefert (Was absolut empfehlenswert ist), müssen zusätzlich noch die Werte in den Zeilen `CMD_PROTOCOL_USESSL=false` und `CMD_HSTS_ENABLE=false` jeweils auf `true` gesetzt werden.

Mit dem Kommando `docker-compose up` in diesem Verzeichnis kann nun der Server-Verbund aus einem Datenbank- und dem HedgeDoc-Server gestartet werden. Beim ersten Start werden alle benötigten Images von `quay.io` heruntergeladen, entpackt, entsprechend den Angaben in `docker-compose.yml` "zusammengebaut" gestartet.

Sobald die Ausgabe `HTTP Server listening at 0.0.0.0:3000` sichtbar ist, kann die HedgeDoc-Seite direkt auf dem Server über [http://localhost:3000](http://localhost:3000) aufgerufen werden.

Um den HedgeDoc-Server wieder zu stoppen, in der Shell einmal `Strg` + `C` drücken und das herunterfahren abwarten.

### HedgeDoc-Server starten und herunterfahren

Der normale Start, damit die Shell nicht blockiert wird, geht im sogenannten `detached`-Modus. Dazu wieder in das Verzeichnis `/opt/hedgedoc-container` wechseln und den Start über das Kommando `docker-compose up -d` veranlassen.

HedgeDoc stoppen erfolgt dann, erneut im Verzeichnis `/opt/hedgedoc-container`, mit dem Kommando `docker-compose down`.

Im normalen Betrieb muss HedgeDoc aber nicht gestoppt und neu gestartet werden. Die laufenden Docker-Container werden beim Herunterfahren des Servers gestoppt, und bei einem Neustart auch wieder automatisch gestartet.

## HedgeDoc über das Netz ansprechen

Um nicht nur auf dem Server selbst, über [http://localhost:3000](http://localhost:3000) arbeiten zu können, wird Nginx oder Apache als Reverse-Proxy vorgeschaltet. Auf Basis der [zugehörigen Anleitung](https://docs.hedgedoc.org/guides/reverse-proxy/) kann also z.B. ein Nginx installiert werden ...

`apt -y install nginx-full ssl-cert`

Dann die Datei `/etc/nginx/sites-available/hedgedoc.conf` mit folgendem Inhalt anlegen:

```
map $http_upgrade $connection_upgrade {
        default upgrade;
        ''      close;
}
server {
        server_name hedgedoc.lfb.local;

        location / {
                proxy_pass http://127.0.0.1:3000;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
        }

        location /socket.io/ {
                proxy_pass http://127.0.0.1:3000;
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $connection_upgrade;
        }

    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    ssl_certificate /etc/ssl/certs/ssl-cert-snakeoil.pem;
    ssl_certificate_key /etc/ssl/private/ssl-cert-snakeoil.key;
    # include options-ssl-nginx.conf;
    # ssl_dhparam ssl-dhparams.pem;
}
```

**ACHTUNG:** Die folgenden Zeilen sind hier verändert ...
- `server_name` gibt den DNS-Namen der Maschine an über die HedgeDoc erreicht wird.
- Die beiden `ssl_certificate`-Zeilen verweisen auf ein selbstsigniertes Zertifikat für Testzwecke
- Die beiden letzten Zeilen `include ...` und `ssl_dhparam ...` sind auskommentiert, müssten aber zusammen mit einem echten Zertifikat dann auch später wieder passend aktiviert werden.

Diese Datei im Verzeichnis `/etc/ngingx/sites-enabled` mit dem Befehl `ln -s /etc/nginx/sites-available/hedgedoc.conf` verlinken und den Nginx mit `systemctl restart nginx` neu starten.

In der `docker-compose.yml` noch folgend Variablen anpassen:

- `CMD_DOMAIN=hedgedoc.lfb.local`
- `CMD_PROTOCOL_USESSL=true`
- `CMD_URL_ADDPORT=false`

und HedgeDoch mit `docker-compose down && docker-compose up -d` neu starten.

Wenn die Namensauflösung funktioniert, ist der Server nun im Netz über [https://hedgedoc.lfb.local/](https://hedgedoc.lfb.local/) erreichbar.

### To-Do bei Netz-Zugriff

Echtes SSL-/TLS-Zertifikat erstellen lassen - z.B. via Lets encrypt und Nginx in `hedgedoc.conf` damit konfigurieren.

## HedgeDoc-Docker konfigurieren

In der `docker-compose.yml` den Bereich `app` -> `environment` lokalisieren. Dort sind ja schon ein paar wenige Variablen definiert - z.B. `CMD_DOMAIN` (siehe oben). Hier können, für die Konfiguration bei Nutzung des Docker-Containers, weitere dieser Umgebungsvariablen eingefügt und eingestellt werden. Welche Einstellungen möglich sind entnimmt man diesen Quellen (**Beachte:** Mit Docker ist die Spalte `environment` relevant):

- [Alle verfügbaren Variablen](https://docs.hedgedoc.org/configuration/)
- [Mehrere Anleitungen für bestimmte Aufgaben (siehe vertikale Menüleiste)](https://docs.hedgedoc.org/guides/reverse-proxy/)

## Nice to know ...

### Wo sind die Daten?

Die vom Docker-Container ausgelagerten Daten (damit das Beenden oder ein Update die Daten nicht vernichtet) findet man unter `/var/lib/docker/volumes/...` in zwei Unterverzeichnissen die den Volumes aus der `docker-compose.yml` entsprechen.

### User-Registrierung gesperrt, manuelle Accounts

Hat man keine externe Authentifizierungsquelle für User (z.B. LDAP), möchte man die unkontrollierte Registrierung von Benutzer:innen häufig auch abschalten. Das geht wieder in der `docker-compose.yml` mit der Environment-Variablen `CMD_ALLOW_EMAIL_REGISTER=false`.

In dem Fall kann man User aber nach wie vor per Command-Line-Interface (CLI) manuell anlegen. Das Kommando befindet sich aber **im** HedgeDoc-Docker-Container! Nachfolgendes Beispiel zeigt wie man in den Container zum dortigen CLI kommt, und das relevante Kommando `manage_users` aufruft. Das Code-Beispiel zeigt auch die Möglichkeiten des Tools.

```
root@deb11vm:~# docker exec -it hedgedoc-container_app_1 /bin/bash
root@2d832153bc37:/hedgedoc# bin/manage_users 
You did not specify either --add or --del or --reset!

Command-line utility to create users for email-signin.

Usage: bin/manage_users [--pass password] (--add | --del) user-email
        Options:
                --add   Add user with the specified user-email
                --del   Delete user with specified user-email
                --reset Reset user password with specified user-email
                --pass  Use password from cmdline rather than prompting
```

Möchte man Gast-Notizen abschalten, so kann das über die Umgebungsvariable `CMD_ALLOW_ANONYMOUS=false` realisiert werden.
